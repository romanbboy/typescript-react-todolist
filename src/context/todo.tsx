import React from "react";
import {ITodoContext} from "../types/context";

// export const TodoContext = React.createContext<Partial<TodoContextType>>({});
export const TodoContext = React.createContext<ITodoContext>({
  dbTodo: [],
  dispatch: () => {}
});