export interface IListType {
  title: string
}

export interface ITodo {
  id: number,
  title: string,
  ready: boolean
}