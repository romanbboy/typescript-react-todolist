import {ITodo} from "./todo";

export interface ITodoContext {
  dbTodo: Array<ITodo>
  dispatch: any
}