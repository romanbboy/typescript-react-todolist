import React, {ChangeEvent, useContext, useState} from 'react'
import {Link} from 'react-router-dom'
import {TodoContext} from "../../context/todo";
import "./index.scss"
import {ITodo} from "../../types/todo";
import {ITodoContext} from "../../types/context";

const TodoItem: React.FC<{todo: ITodo}> = ({todo}) => {
  const [edit, setEdit] = useState<boolean>(false);
  const [editValue, setEditValue] = useState<string>(todo.title)

  const [ready, setReady] = useState<boolean>(todo.ready)

  const {dbTodo, dispatch} = useContext<ITodoContext>(TodoContext)
  
  const deleteTodo = (): void => {
    dispatch(dbTodo.filter((el: ITodo) => el.id !== todo.id))
  }

  const editTodo = (): void => {
    let updateDb: ITodo[] = dbTodo.map((el: ITodo) => {
      let currentEl: ITodo = {...el};
      if (el.id === todo.id){
        currentEl.title = editValue;
        currentEl.ready = ready;
      }
      return currentEl;
    })

    dispatch(updateDb);
    setEdit(false)
  }

  return (
    <div className="todo">
      {edit ? (<>
        <input type="text" value={editValue} onChange={(e: ChangeEvent<HTMLInputElement>) => setEditValue(e.target.value)}/>
        <div>
          <input type="checkbox" checked={ready} onChange={(e: ChangeEvent<HTMLInputElement>) => setReady(e.target.checked)}/> &nbsp; &nbsp;
          <button onClick={editTodo}>Применить</button>
        </div>
      </>) : (<>
        <div className="todo__name">
          <Link to={`/todo/${todo.id}`} >{todo.title}</Link>
          {ready ? '(Готово)' : ''}
        </div>
        <div className="todo__actions">
          <button onClick={() => setEdit(true)}>Edit</button>
          <button onClick={deleteTodo}>Delete</button>
        </div>
      </>)}
    </div>
  )
}

export default TodoItem