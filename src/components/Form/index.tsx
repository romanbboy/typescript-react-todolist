import React, {useState, useContext, ChangeEvent} from 'react';
import './index.scss'
import {TodoContext} from "../../context/todo";
import {ITodoContext} from "../../types/context";
import {IFormProps} from "../../types/form";

const Form: React.FC<IFormProps> = ({separate = false}) => {
  const {dbTodo, dispatch} = useContext<ITodoContext>(TodoContext);
  const [val, setVal] = useState<string>('')

  const addTodo = (): void => {
    if (val) {
      dispatch([...dbTodo, {id: +new Date, title: val, ready: false}])
      setVal('')
    }

  }

  return (
    <div className="Form">
      <input type="text" value={val} onChange={(e:ChangeEvent<HTMLInputElement>) => setVal(e.target.value)}/>

      {separate && <>&nbsp;&nbsp;&nbsp;</>}

      <button onClick={addTodo}>Добавить</button>
    </div>
  );
}

export default Form