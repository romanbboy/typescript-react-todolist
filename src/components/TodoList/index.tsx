import React, {useContext} from 'react'
import TodoItem from "../TodoItem";
import {TodoContext} from "../../context/todo";
import "./index.scss"
import {ITodo} from "../../types/todo";
import {ITodoContext} from "../../types/context";

const TodoList = () => {
  const {dbTodo} = useContext<ITodoContext>(TodoContext);
  
  return (
    <div className="TodoList">
      <div className="TodoList__count">Количество задач: {dbTodo.length}</div>
      {dbTodo.map((todo: ITodo) => <TodoItem key={todo.id} todo={todo} />)}
    </div>
  )
}

export default TodoList