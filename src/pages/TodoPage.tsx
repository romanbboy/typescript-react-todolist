import React from 'react'
import {RouteComponentProps} from 'react-router'
import { useHistory } from "react-router-dom";
import {IRouteParams} from "../types/route";

const TodoPage = ({match}: RouteComponentProps<IRouteParams>) => {
  const history = useHistory();

  return (
    <div className="TodoPage">
      TodoPage - {match.params.id}

      <button onClick={history.goBack}>Back</button>
    </div>
  )
}

export default TodoPage