import React, {useState} from 'react'
import {TodoContext} from "../context/todo";
import TodoList from "../components/TodoList";
import Form from "../components/Form";
import {ITodo} from "../types/todo";

function MainPage(){
  const [dbTodo, setDbTodo] = useState<ITodo[]>([]);


  return (
    <div className="MainPage">
      <h1 style={{textAlign: 'center', marginBottom: '30px'}}>Список дел</h1>

      <TodoContext.Provider value={{dbTodo, dispatch: setDbTodo }} >
        <TodoList />
        <Form separate={true}/>
      </TodoContext.Provider>
    </div>
  )
}

export default MainPage