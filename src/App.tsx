import React from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom'
import MainPage from "./pages/MainPage";
import TodoPage from "./pages/TodoPage";

const App = () => {
  return (
    <BrowserRouter>
      <div className="App">
        <Switch>
          <Route exact path="/" component={MainPage} />
          <Route exact path="/todo/:id" component={TodoPage} />
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
